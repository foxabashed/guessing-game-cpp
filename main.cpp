#include <cstdlib>
#include <ctime>
#include <iostream>
#include <climits>
#include <stdexcept>

/*
    Copyright (c) 2024 Ali Yuruk
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

std::string checkIntegerSafety(std::string input) {
    // For loop to loop through every character in the inputted string and determine if they're all digits
    for (char const &c : input) {
        if (!isdigit(c)) {
            std::cout << "Error: Please input a valid positive integer.\n";
            exit(-2);
        }
    }

    // Check if the length of the input string is above 9, so that it can't pass INT_MAX
    int length = input.length();
    if (length > 9) {
        std::cout << "Error: Please input an integer smaller than 10 digits\n";
        exit(-1);
    }

    return input;
}

int main() {
    // Declare chanceCount and guessedNumber as strings to later check their memory safety since they're user inputted
    std::string chanceCount = "";
    std::string guessedNumber = "";

    srand(time(0));
    const int randomNumber = rand() % 101;

    std::cout << "How many chances do you want to be given?\n";
    std::cin >> chanceCount;

    // Check the memory-safety of and convert the memory-safe chanceCount to an integer
    int safeChanceCount = stoi(checkIntegerSafety(chanceCount));

    std::cout << "Guess a number:\n";

    for (int i = 0; i < safeChanceCount; i++) {
        std::cin >> guessedNumber;

        // Check the memory-safety of and convert the memory-safe guessedNumber to an integer
        int safeGuessedNumber = stoi(checkIntegerSafety(guessedNumber));

        if (safeGuessedNumber == randomNumber)  {
            std::cout << "Congrats, you guessed correctly!\n";
            exit(0);
        }
        else if (safeGuessedNumber < randomNumber) {
            std::cout << "The number is too small, try again:\n";
        }
        else if (safeGuessedNumber > randomNumber) {
            std::cout << "The number is too big, try again:\n";
        }
    }
    
    std::cout << "You have reached the maximum amount of " << safeChanceCount << " guesses. You lost!\n";

    return 0;
}